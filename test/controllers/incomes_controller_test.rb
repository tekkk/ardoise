require 'test_helper'

class IncomesControllerTest < ActionDispatch::IntegrationTest

  test "should create income" do
    associate = associates(:one)
    assert_difference('Income.count', 1) do
      post incomes_url, params: {
        income: {
          amount: 100,
          month: 9,
          year: 2018,
          associate_id: associate.id,
          share_model: share_models(:flat).id
        }
      }
    end

    assert_equal 100_00, Income.last.amount

    assert_redirected_to associate_url(associate.id)
  end

  test "should destroy income" do
    assert_difference('Income.count', -1) do
      delete income_url(incomes(:jan))
    end

    assert_redirected_to associate_url(associates(:one).id)
  end
end
