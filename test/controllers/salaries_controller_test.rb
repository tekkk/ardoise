require 'test_helper'

class SalariesControllerTest < ActionDispatch::IntegrationTest

  test "should create salary" do
    associate = associates(:one)
    assert_difference('Salary.count', 1) do
      post salaries_url, params: {
        salary: {
          net_pay: 1,
          employee_contribution: 2,
          employer_contribution: 3,
          saving_plan_contribution: 4,
          associate_id: associate.id
        }
      }
    end

    assert_equal 100, Salary.last.net_pay
    assert_equal 200, Salary.last.employee_contribution
    assert_equal 300, Salary.last.employer_contribution
    assert_equal 400, Salary.last.saving_plan_contribution

    assert_redirected_to associate_url(associate.id)
  end

  test "should destroy salary" do
    assert_difference('Salary.count', -1) do
      delete salary_url(salaries(:one))
    end

    assert_redirected_to associate_url(associates(:one).id)
  end
end
