require 'test_helper'

class IncomeTest < ActiveSupport::TestCase
  setup do
    @associate = Associate.create(display_name: "Me", email: "me@example.org")
    @model = ShareModel.create()
  end

  test "should create an income" do
    income = Income.new
    income.associate = @associate
    income.share_model = @model
    income.amount = 2000
    income.month = 9
    income.year = 2018
    income.label = "Income"
    assert income.valid?
  end

  test "should not create an income without month" do
    income = Income.new
    income.associate = @associate
    income.share_model = @model
    income.amount = 2000
    income.label = "Income"
    assert_not income.valid?
  end

  test "should not create an income without amount" do
    income = Income.new
    income.associate = @associate
    income.share_model = @model
    income.month = 9
    income.year = 2018
    income.label = "Income"
    assert_not income.valid?
  end

  test "should require a positive amount" do
    income = Income.new
    income.associate = @associate
    income.share_model = @model
    income.amount = -40
    income.month = 9
    income.year = 2018
    income.label = "Income"
    assert_not income.valid?
  end

  test "should require an associate" do
    income = Income.new
    income.share_model = @model
    income.amount = 2000
    income.month = 9
    income.year = 2018
    income.label = "Income"
    assert_not income.valid?
  end
end
