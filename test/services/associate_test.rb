require 'test_helper'

class AssociateServiceTest < ActiveSupport::TestCase
  test 'should extrapolate the incomes' do
    jan = incomes(:jan)
    assert_equal 3_000_00 / 12, AssociateService.total_contribution([jan])
  end

  test 'should extrapolate over two different months of the same year' do
    jan = incomes(:jan)
    feb = incomes(:feb)
    assert_equal 3_000_00 / 6, AssociateService.total_contribution([jan, feb])
  end

  test 'should extrapolate over the same month, different year' do
    j17 = incomes(:jan_2017)
    j18 = incomes(:jan)
    assert_equal 3_000_00 / 6, AssociateService.total_contribution([j17, j18])
  end

  test 'should cummmulate the incomes of the same month' do
    jan = incomes(:jan)
    jan_extra = incomes(:jan_extra)
    assert_equal 1_000_00, AssociateService.total_contribution([jan, jan_extra])
  end

  test 'should return all wanted data' do
    incomes = [incomes(:jan), incomes(:feb)] # 10_000_00
    spendings = [spendings(:one), spendings(:two)] # 3
    salaries = [salaries(:one), salaries(:two)]

    result = AssociateSummary.new(incomes, salaries, spendings)

    assert_equal 10_000_00, result.incomes
    assert_equal 3_000_00 / 6, result.contributions
    assert_equal 3, result.spendings
    assert_equal 300_00, result.net_pay
    assert_equal 30_00, result.employee_contribution
    assert_equal 3_00, result.employer_contribution
    assert_in_delta 30 * 3 * (1 - 0.08), result.employer_matching
    assert_in_delta 916614.20, result.net_balance
  end

  test 'should not suggest a negative bonus' do
    assert_equal 0, AssociateSummary.compute_bonus(-1000)
  end

  test 'should compute the suggested bonus' do
    assert_in_delta 1000_00, AssociateSummary.compute_bonus(3660_00)
  end

end
