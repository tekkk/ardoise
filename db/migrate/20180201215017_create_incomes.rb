class CreateIncomes < ActiveRecord::Migration[5.1]
  def change
    create_table :incomes do |t|
      t.belongs_to :associate, index: true
      t.integer :amount
      t.integer :month
      t.integer :year
      t.string :label

      t.timestamps
    end
  end
end
