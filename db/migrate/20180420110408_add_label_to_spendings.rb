class AddLabelToSpendings < ActiveRecord::Migration[5.1]
  def change
    add_column :spendings, :label, :string
  end
end
