class CreateSalaries < ActiveRecord::Migration[5.1]
  def change
    create_table :salaries do |t|
      t.integer :year
      t.integer :month
      t.integer :net_pay
      t.integer :employee_contribution
      t.integer :employer_contribution
      t.integer :saving_plan_contribution
      t.references :associate, foreign_key: true

      t.timestamps
    end
  end
end
