class CreateSpendings < ActiveRecord::Migration[5.1]
  def change
    create_table :spendings do |t|
      t.integer :year
      t.integer :month
      t.integer :amount
      t.references :associate, foreign_key: true

      t.timestamps
    end
  end
end
