class AddOauthProviderToAssociates < ActiveRecord::Migration[5.1]
  def change
    add_column :associates, :oauth_provider, :string
    add_column :associates, :oauth_uid, :string
    add_column :associates, :access, :boolean
  end
end
