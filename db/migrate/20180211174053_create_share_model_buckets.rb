class CreateShareModelBuckets < ActiveRecord::Migration[5.1]
  def change
    create_table :share_model_buckets do |t|
      t.integer :lower_bound
      t.float :rate
      t.references :share_model, foreign_key: true

      t.timestamps
    end
  end
end
