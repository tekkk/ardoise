class CreateShareModels < ActiveRecord::Migration[5.1]
  def change
    create_table :share_models do |t|
      t.text :label

      t.timestamps
    end
  end
end
