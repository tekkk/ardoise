class SalariesController < ApplicationController
  # POST /salaries
  def create
    params = salary_params
    params[:net_pay] = params[:net_pay].to_cents
    params[:employee_contribution] = params[:employee_contribution].to_cents
    params[:employer_contribution] = params[:employer_contribution].to_cents
    params[:saving_plan_contribution] = params[:saving_plan_contribution].to_cents
    params[:associate] = Associate.find(params[:associate_id])
    @salary = Salary.new(params)

    if @salary.save
      flash.notice = 'Ajout d’un salaire avec succès'
    else
      flash.alert = 'Erreur à l’ajout d’un salaire'
    end

    redirect_to @salary.associate
  end

  def destroy
    @salary = Salary.find(params[:id])
    associate = @salary.associate
    @salary.destroy
    flash.notice = 'Salaire supprimé'
    redirect_to associate
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def salary_params
      params.require(:salary).permit(:year, :month, :net_pay, :employee_contribution, :employer_contribution, :saving_plan_contribution, :associate_id)
    end
end
