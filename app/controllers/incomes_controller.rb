class IncomesController < ApplicationController
  def create
    params = income_params
    params[:amount] = params[:amount].to_cents
    params[:associate] = Associate.find(params[:associate_id])
    params[:share_model] = ShareModel.find(params[:share_model])

    @income = Income.new(params)

    if @income.save
        flash.notice = 'Ajout du revenu avec succès'
    else
        flash.alert = 'Erreur à l’ajout d’un revenu'
    end

    redirect_to @income.associate
  end

  def destroy
    @income = Income.find(params[:id])
    associate = @income.associate
    @income.destroy
    flash.notice = 'Revenu supprimé'
    redirect_to associate
  end

  private

  def income_params
      params.require(:income).permit(:label, :associate, :month, :year, :amount, :associate_id, :share_model)
  end
end
