class AssociatesController < ApplicationController
    before_action :set_associate, only: [:show, :edit, :update, :destroy]

    def show
        @associate = Associate.find(params[:id])
        @summary = @associate.summary
    end

    def index
        @associate = Associate.all
    end


    def new
        @associate = Associate.new
    end

    def create
        @associate = Associate.new(associate_params)

        @associate.save
        redirect_to @associate
    end

    def edit
    end

    def update
        if @associate.update(associate_params)
            redirect_to @associate, notice: 'Associé·e mis à jour.'
        else
            render :edit
        end
    end

    private

    def set_associate
        @associate = Associate.find(params[:id])
    end

    def associate_params
        params.require(:associate).permit(:display_name, :email, :access)
    end
end
