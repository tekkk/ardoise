class SessionsController < ApplicationController
    def create
        @associate = Associate.from_omniauth(request.env['omniauth.auth'])
        session[:user_id] = @associate.id
        flash[:success] = "Bienvenue, #{@associate.display_name}!"
        
        redirect_to root_path
    end

    def destroy
        if current_associate
          session.delete(:user_id)
          flash[:success] = 'Au revoir !'
        end
        redirect_to root_path
    end
end
