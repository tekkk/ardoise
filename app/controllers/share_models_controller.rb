class ShareModelsController < ApplicationController
  before_action :set_share_model, only: [:show, :edit, :update, :destroy]

  # GET /share_models
  def index
    @share_models = ShareModel.all
  end

  # GET /share_models/1
  def show
  end

  # GET /share_models/new
  def new
    @share_model = ShareModel.new
  end

  # GET /share_models/1/edit
  def edit
  end

  # POST /share_models
  def create
    @share_model = ShareModel.new(share_model_params)

    if @share_model.save
      redirect_to @share_model, notice: 'Modèle de répartition créé.'
    else
      render :new
    end
  end

  # PATCH/PUT /share_models/1
  def update
    if @share_model.update(share_model_params)
      redirect_to @share_model, notice: 'Modèle de répartition mis à jour.'
    else
      render :edit
    end
  end

  # DELETE /share_models/1
  def destroy
    @share_model.destroy
    redirect_to share_models_url, notice: 'Modèle de répartion supprimé.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_share_model
      @share_model = ShareModel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def share_model_params
      params.require(:share_model).permit(:label)
    end
end
