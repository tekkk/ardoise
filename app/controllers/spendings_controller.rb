class SpendingsController < ApplicationController
  # POST /spendings
  def create
    params = spending_params
    params[:amount] = params[:amount].to_cents
    params[:associate] = Associate.find(params[:associate_id])
    @spending = Spending.new(params)

    if @spending.save
      flash.notice = 'Ajout d’une dépense avec succès'
    else
      flash.alert = 'Erreur à l’ajout d’une dépense'
    end

    redirect_to @spending.associate
  end

  def destroy
    @spending = Spending.find(params[:id])
    associate = @spending.associate
    @spending.destroy
    flash.notice = 'Dépense supprimée'
    redirect_to associate
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def spending_params
      params.require(:spending).permit(:year, :month, :amount, :associate_id, :label)
    end
end
