class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def current_associate
    @current_associate ||= Associate.find_by(id: session[:user_id])
  end

  helper_method :current_associate

end
