class WelcomeController < ApplicationController
  def index
    if current_associate
      redirect_to current_associate
    end
  end
end
