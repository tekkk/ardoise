class ShareModelBucketsController < ApplicationController

    def create
        params = share_model_bucket_params
        params["lower_bound"] = params["lower_bound"].to_cents
        params["rate"] = params["rate"].to_f / 100
        @share_model_bucket = ShareModelBucket.new(params)

        if @share_model_bucket.save
            flash.notice = 'Ajout d’une tranche avec succès'
        else
            flash.alert = 'Erreur à l’ajout d’une tranche'
        end
        redirect_to @share_model_bucket.share_model
    end

    def share_model_bucket_params
        params.require(:share_model_bucket).permit(:lower_bound, :rate, :share_model_id)
    end
end
