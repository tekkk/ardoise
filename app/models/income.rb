class Income < ApplicationRecord
  validates :month, presence: true
  validates :year, presence: true
  validates :amount, presence: true
  validates :amount, numericality: { greater_than: 0 }
  belongs_to :associate
  belongs_to :share_model
end
