class Salary < ApplicationRecord
  belongs_to :associate

  def employer_matching
    contribution_rate = 3.00
    csg_rate = self.year <= 2017 ? 0.08 : 0.097

    self.saving_plan_contribution * contribution_rate * (1 - csg_rate)
  end
end
