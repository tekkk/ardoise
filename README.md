**Ardoise** est l’outil de répartition du chiffre d’affaires entre les associés de Codeurs en Liberté. 

## Développer en local

1. Installer ruby (de préférence avec [rbenv](https://github.com/rbenv/rbenv#installation)),
2. `make install` pour installer les dépendances,
3. Générer les tokens OAuth:
  * Créer une application "Ardoise" dans GitLab (`Profile Settings ▸ Applications`),
  * Copier les tokens générés dans `config/local_env.yml`.
4. `make run` pour lancer le serveur.

## Tester

`make test`

## Déployer

1. Installer la [CLI clever-cloud](https://www.clever-cloud.com/doc/clever-tools/getting_started/)
   * Sur mac, `brew install CleverCloud/tap/clever-tools`
2. `make deploy`
