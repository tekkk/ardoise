class String
  def to_cents
    (self.to_f * 100).to_i
  end
end
