require 'money'

class Numeric
  def displayed_euros
    Money.new(self, "EUR").format(:symbol_position => :after)
  end
end
