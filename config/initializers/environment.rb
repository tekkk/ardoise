# Ensure that the environment variables defined in local_env.example.yml
# are present in the execution environment.
#
# This protects against out-to-date environment leading to runtime errors.

if ENV['RAILS_ENV'] != 'test' && File.basename($0) != 'rake'
  reference_env_file = File.join(Rails.root, 'config', 'local_env.example.yml')
  YAML.load(File.open(reference_env_file)).each do |key, value|
    if ENV[key.to_s].blank?
      raise "Configuration error: `#{key}` is not present in the process environment variables (declared in `config/local_env.example.yml`)"
    end
  end
end
