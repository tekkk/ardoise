Rails.application.routes.draw do
  resources :share_model_buckets
  resources :share_models
  resources :salaries
  resources :spendings
  get 'welcome/index'

  resources :associates
  resources :incomes

  root 'welcome#index'

  get '/auth/:provider/callback', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
end
