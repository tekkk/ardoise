install: config/local_env.yml ## Install or update the required dependencies
	gem list --silent --installed bundler || gem install bundler
	bundle check || bundle install

run: install db/development.sqlite3 ## Start the server
	bin/rails server

test: ## Execute all tests
	bin/rails test

deploy:
	clever deploy

help: ## Display available commands
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

# Private: create the config file
config/local_env.yml:
	cp config/local_env.example.yml config/local_env.yml

# Private: initialize the development database
db/development.sqlite3:
	bin/rails db:create
	bin/rails db:migrate
	bin/rails db:seed

.PHONY: install run test help
.DEFAULT_GOAL := help
